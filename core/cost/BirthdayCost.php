<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 11:59 PM
 */

namespace core\cost;

class BirthdayCost implements CalculatorInterface
{
    private $next;
    private $percent;
    private $birthDate;
    private $currentDate;

    public function __construct(CalculatorInterface $next, $percent, $birthDate, $currentDate)
    {
        $this->next = $next;
        $this->percent = $percent;
        $this->birthDate = $birthDate;
        $this->currentDate = $currentDate;
    }

    public function getCost(array $items)
    {

        $birthDate = \DateTime::createFromFormat('Y-n-d', $this->birthDate);
        $currentDate = \DateTime::createFromFormat('Y-n-d', $this->currentDate);

        if ($currentDate->format('m-d') == $birthDate->format('m-d')) {
            return (1 - $this->percent / 100) * $this->next->getCost($items);
        } else {
            return $this->next->getCost($items);
        }
    }

}
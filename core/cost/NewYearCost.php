<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 11:51 PM
 */

namespace core\cost;

class NewYearCost implements CalculatorInterface
{
    private $next;
    private $month;
    private $percent;

    public function __construct(CalculatorInterface $next, \DateTime $month, $percent)
    {
        $this->month = $month;
        $this->percent = $percent;
        $this->next = $next;
    }

    public function getCost(array $items)
    {

        $cost = $this->next->getCost($items);
        if ($this->month == 12) {
            return (1 - $this->percent / 100) * $cost;
        } else {
            return $cost;
        }
    }

}
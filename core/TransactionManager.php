<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 3/12/18
 * Time: 3:33 PM
 */

namespace core;

class TransactionManager
{
    private $transaction;

    public function __construct(TransactionInterface $transaction)
    {
        $this->transaction = $transaction;
    }

    public function execute(callable $function)
    {
        $this->transaction->beginTransaction();
        try {
            call_user_func($function);
            $this->transaction->commit();
        } catch (\Exception $exception) {
            $this->transaction->rollBack();
            throw $exception;
        }
    }

}
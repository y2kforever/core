<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 11:26 PM
 */

namespace core\storage;

class DbStorage implements StorageInterface
{
    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function load()
    {
    }

    public function save(array $items)
    {
    }

}
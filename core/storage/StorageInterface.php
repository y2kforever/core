<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 11:23 PM
 */

namespace core\storage;

interface StorageInterface
{
    public function load();

    public function save(array $items);
}
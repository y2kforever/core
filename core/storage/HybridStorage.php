<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 12:24 PM
 */

namespace core\storage;

class HybridStorage implements StorageInterface
{
    private $storage;

    public function __construct(StorageInterface $from, StorageInterface $to)
    {
        $items = array_merge($from->load(), $to->load());
        $from->save([]);
        $to->save($items);
        $this->storage = $to;
    }

    public function load()
    {
        return $this->storage->load();
    }

    public function save(array $items)
    {
        $this->storage->save($items);
    }

}